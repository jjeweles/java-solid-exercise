package com.galvanize.formatters;

import com.galvanize.Calendar;

public interface FormatInterface {
    String format(Calendar calendar);
}
