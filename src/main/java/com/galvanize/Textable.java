package com.galvanize;

public interface Textable {

    String getTextToDisplay();

}
