package com.galvanize;

public interface Completable extends Textable {

    void markComplete();

    void markIncomplete();

    boolean isComplete();

}
