package com.galvanize;

import java.time.LocalDateTime;

public interface Schedulable extends Textable {

    String iCalendar();

    LocalDateTime getStartsAt();

}
